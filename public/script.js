function gotofile(filename){
    let isSafari = navigator.vendor.match(/apple/i) &&
                 !navigator.userAgent.match(/crios/i) &&
                 !navigator.userAgent.match(/fxios/i) &&
                 !navigator.userAgent.match(/Opera|OPT\//);
    
    
    if (isSafari) {
      window.open(`pdf/${filename}.pdf`);
    } else {
      window.open(`viewer.html?file=pdf/${filename}.pdf`);
    }
}


function redirect(link){
    window.location.href = link;

}